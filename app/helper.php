<?php

if (!function_exists('generateFilterUrl')) {
    function generateFilterUrl($params)
    {
        //In real project, this settings need moved to the config file
        $default = [
            "pagesize" => 20,
            "year"  => 'asc',
            "price" => 'asc'
        ];

        $url = URL::full();
        if (!is_array($params) || empty($params)) {
            return $url;
        }

        $query = [];
        $parts = parse_url($url);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $query);
        }

        foreach ($params as $key => $value) {
            if (isset($default[$key]) && $value == $default[$key]) {
                unset($params[$key]);
            }

            if (array_key_exists($key, $query)) {
                unset($query[$key]);
            }

            if ($i = array_search($value, $query)) {
                unset($query[$i]);
            }

            if (isset($query['page'])) {
                unset($query['page']);
            }
        }

        $query = array_merge($query, $params);
        if (empty($query)) {
            return URL::current();
        }

        return URL::current() . "?" . http_build_query($query, '', '&amp;');
    }
}
