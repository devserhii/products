<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class Filter
{
    protected $req;
    protected $builder;

    public function __construct(Request $req)
    {
        $this->req = $req;
    }

    public function apply(Builder $builder, $only)
    {
        $this->builder = $builder;
        $params = $this->req->all();

        if (!empty($only)) {
            $params = $this->req->only($only);
        }

        foreach ($params as $name => $value) {
            if (empty($value)) {
                continue;
            }
            if (method_exists($this, $name)) {
                call_user_func_array([$this, $name], array_filter([$value]));
            }
        }

        return $this->builder;
    }

}