<?php

namespace App\Filters;

class ProductFilters extends Filter
{
    public function year($value)
    {
        return $this->orderBy("year", $value);
    }

    public function price($value)
    {
        return $this->orderBy("price", $value);
    }

    private function orderBy($field, $value)
    {
        $orderValue = ($value == 'desc') ? $value : 'asc';
        return $this->builder->orderBy($field, $orderValue);
    }
}