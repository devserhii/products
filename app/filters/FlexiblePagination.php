<?php

namespace App\Filters;

use Illuminate\Support\Facades\Input;

trait FlexiblePagination
{
    public function getPageSize()
    {
        $pageSize = (int)Input::get('pagesize');
        if (!$pageSize || !in_array($pageSize, [10, 20, 50])) {
            return 20;
        }
        return $pageSize;
    }

}