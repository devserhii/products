<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

trait Filterable
{
    public function scopeFilter(Builder $query, Filter $filter, $only = [])
    {
        return $filter->apply($query, $only);
    }
}