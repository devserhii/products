<?php


class ProductsTableSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];

        for ($i = 0; $i < 100; $i++) {
            $data[] = [
                "name" => $faker->state,
                "price" => rand(10,500),
                "description" => $faker->text,
                "year" => $faker->year,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now()
            ];
        }

        Products::insert($data);

        $this->command->info('Products table seeded!');
    }

}