@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Products</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">{{ $paginator }}</div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th>
                            <div class="filter">
                                <div class="ml-1">
                                    <div class="dropdown">
                                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButtonRow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ $pageSize }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonRow">
                                            @foreach([10,20,50] as $item)
                                                <a class="dropdown-item" href="{{ generateFilterUrl(["pagesize"=> $item]) }}">{{ $item }}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </th>
                        <th>
                            <div class="btn btn-secondary btn-sm">
                                Name
                            </div>
                        </th>
                        <th>
                            <div class="filter">
                                <div class="ml-1">
                                    <div class="dropdown">
                                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButtonPrice" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Price
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonName">
                                                <a class="dropdown-item" href="{{ generateFilterUrl(["price"=> "asc"]) }}">Min</a>
                                                <a class="dropdown-item" href="{{ generateFilterUrl(["price"=> "desc"]) }}">Max</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </th>
                        <th>
                            <div class="btn btn-secondary btn-sm">
                                Description
                            </div>
                        </th>
                        <th>
                            <div class="filter">
                                <div class="ml-1">
                                    <div class="dropdown">
                                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButtonYear" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Year
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonYear">
                                            <a class="dropdown-item" href="{{ generateFilterUrl(["year"=> "asc"]) }}">Old first</a>
                                            <a class="dropdown-item" href="{{ generateFilterUrl(["year"=> "desc"]) }}">New first</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </th>
                        <th>
                            <div class="btn btn-secondary btn-sm">
                                Date
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ number_format($product->price) }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->year }}</td>
                        <td>{{ $product->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col">
            {{ $paginator }}
        </div>
    </div>


@stop