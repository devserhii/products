<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    {{HTML::style('libs/bootstrap/css/bootstrap.min.css')}}
    {{HTML::style('css/main.css')}}
    <title>Products - Test</title>
</head>
<body>
<div class="container">
    @yield('content', 'Default Content')
</div>

{{ HTML::script('libs/jquery-3.4.1.min.js') }}
{{ HTML::script('libs/popper.min.js') }}
{{ HTML::script('libs/bootstrap/js/bootstrap.min.js') }}

</body>
</html>
