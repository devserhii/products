<?php

use App\Filters\FlexiblePagination;
use App\Filters\ProductFilters;

class ProductsController extends BaseController
{

    use FlexiblePagination;

    private $filters;

    public function __construct(ProductFilters $filters)
    {
        $this->filters = $filters;
    }

    public function index()
    {
        $pageSize = $this->getPageSize();
        $products = Products::filter($this->filters, ["year", "price"])->paginate($pageSize);
        $paginator = $products->appends(Input::except('page'))->links();

        return View::make('products', compact('products', 'paginator', 'pageSize'));
    }

}
