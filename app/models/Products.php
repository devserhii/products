<?php

use App\Filters\Filterable;

class Products extends Eloquent {

    use Filterable;

	protected $table = 'products';

    protected $fillable = array('name', 'price', 'description','year');

}
